# MineClone Server modpack

A small modpack for MCL(2/5/A), that contains some basic mods for multiplayer server, like mods to protect territory and stuff, to /sit and /lay, to do /rtp, to see playerlist and so on.

## Note

This modpack is just a bunch of submodules to make my work easier, so to clone this repo properly do:
```
git clone --recurse-submodules https://git.minetest.land/rudzik8/mcl_serverpack.git
```

To pull this repo properly do:
```
git pull --recurse-submodules
```
Thanks!

## Included mods

- [**areas**](https://github.com/minetest-mods/areas) *by ShadowNinja and contibutors* - protect your territory with commands (something like `/rg` from MC)
- [**mcl_cozy**](https://git.minetest.land/rudzik8/mcl_cozy) *by everamzah and rudzik8* - sit and lay using commands (my fork of old cozy mod)
- [**playerlist**](https://github.com/EliasFleckenstein03/playerlist) *by EliasFleckenstein* - see list of players online on sneak (just like MC tab-list)
- [**random_teleport**](https://github.com/Droog71/random_teleport) *by Droog71* - teleport to randomly choosen place on the map (to quickly find place for home for example)

## License

Each mod there has it's own license (check their repos to see it), but **this** repo (excluding submodules) is licensed under Unlicense. See <https://unlicense.org/> or LICENSE file for details.
